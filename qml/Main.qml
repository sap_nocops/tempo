/*
 * Copyright (C) 2023  Lorenzo Torracchi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * tempo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'tempo.sap'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        id: page
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Tempo')
            trailingActionBar.actions: [
                Action {
                    iconName: "view-refresh"
                    text: i18n.tr("Restart")
                    onTriggered: {
                        page.times = []
                        tempoLabel.text = i18n.tr('Tap screen at tempo')
                    }
                }
            ]
        }

        property var times: []

        Label {
            id: tempoLabel
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            text: i18n.tr('Tap screen at tempo')
            font.pixelSize: units.gu(10)
            wrapMode: Text.WordWrap

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    page.times.push(new Date().getTime())
                }
            }
        }
        Timer{
            id: timer
            running: true
            repeat: true
            interval: 1000
            onTriggered: {
                if (page.times.length < 2) {
                    return
                }
                let timeDiffSum = 0
                for(let i = 1; i < page.times.length; i++) {
                    timeDiffSum += page.times[i] - page.times[i-1]
                }
                tempoLabel.text = parseInt((1000 / (timeDiffSum / (page.times.length - 1))) * 60, 10)
            }
        }
    }
}
